<?php
require_once 'lib/func.inc';

?>

<!DOCTYPE html>

<html lang="en">

<head>

<meta charset="UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1">

<title></title>

<!--  <link href="lib/bootstrap337/css/bootstrap.min.css" rel="stylesheet">  -->
<link href="/app/frontend/web/lib/bootstrap337/css/bootstrap.min.css" rel="stylesheet"> 

</head>

<body>


	<div class="container-fluid">


		<!--  Меню -->

		<div class="row">



			<nav class="navbar navbar-default">

				<div class="container-fluid">

					<!-- Brand and toggle get grouped for better mobile display -->

					<div class="navbar-header">

						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse"
							data-target="#bs-example-navbar-collapse-1" aria-expanded="false">

							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>

						</button>

						<a class="navbar-brand" href="/lics.php">Управление лицензиями</a>



					</div>



					<!-- Collect the nav links, forms, and other content for toggling -->

					<div class="collapse navbar-collapse"
						id="bs-example-navbar-collapse-1">

						<ul class="nav navbar-nav">





							<!-- <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>  -->

							<li><a href="?p=full_report">Полный список</a></li>

							<li><a href="?p=activated_report">Список активированных</a></li>

							<!-- <li><a href="?p=reset_activation">Обнулить активацию</a></li>  -->



							<!-- 
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Отделы<span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="?p=create_dep">Создать</a></li>
		            <li><a href="?p=change_dep">Изменить</a></li>
		            <li><a href="?p=del_dep">Удалить</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="?p=view_all_dep">Просмотреть все</a></li>
		          </ul>
		        </li>
		        
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Должности<span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="?p=create_tit">Создать</a></li>
		            <li><a href="?p=change_tit">Изменить</a></li>
		            <li><a href="?p=del_tit">Удалить</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="?p=view_all_tit">Просмотреть все</a></li>
		          </ul>
		        </li>
		        
		        <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Сотрудники<span class="caret"></span></a>
		          <ul class="dropdown-menu">
		             <li><a href="?p=create_emp">Создать</a></li>
		             <li><a href="?p=on_position">Назначить на должность</a></li>
		            <li><a href="?p=change_emp">Изменить</a></li>
		            <li><a href="?p=del_emp">Уволить</a></li>
		            <li role="separator" class="divider"></li>
		            <li><a href="?p=view_all_emp">Просмотреть все</a></li>
		            <li><a href="?p=view_all_emp_by_tit">По должностям</a></li>
		          </ul>
		        </li>
		        
		        -->

						</ul>

						<form class="navbar-form navbar-left" method="post"
							action="lics.php" enctype="multipart/form-data">

							<div class="form-group">

								<input type="text" class="form-control" placeholder="Поиск"
									id="search_param" name="search_param">

							</div>

							<button type="submit" class="btn btn-default" id="search"
								name="search">Найти</button>

						</form>

						<a class="btn btn-default" href="lics.php?p=generate"
							role="button">Генерировать ключи</a>
							
							<a class="btn btn-default" href="lics.php?p=newkeys"
							role="button">Новые ключи</a>

					</div>
					<!-- /.navbar-collapse -->

				</div>
				<!-- /.container-fluid -->

			</nav>



		</div>

		<!--  Меню -->



		<!--  Форма -->

		<div class="row">
	  	<?php
				
				if (! isset ( $_SESSION ["logged"] )) {
					
					require_once 'login.php';
				} else {
					
					if (! isset ( $_GET ['p'] )) {
						
						if (isset ( $_POST ['search'] )) {
							
							echo '
    		<div class="col-xs-12">
	  			<h4>Результаты поиска</h4>
	  			<hr>';
							
							search_lic ( $_POST ['search_param'] );
						} else {
							
							echo '<div class="col-xs-6">';
							
							alert_success ( "Добро пожаловать в систему учёта лицензий" );
						}
					} else {
						
						switch ($_GET ['p']) {
							
							case 'full_report' :
								
								echo '<div class="col-xs-12">
		        		<h4>Полный список лицензий</h4>
						<hr>';
								
								view_full_report ();
								
								break;
							
							case 'activated_report' :
								
								echo '<div class="col-xs-12">
		        		<h4>Список активированных лицензий</h4>
						<hr>';
								
								view_activated_report ();
								
								break;
							
							case 'reset_activation' :
								
								echo '<div class="col-xs-12">
		        	<h4>Обнулить активацию</h4>
					<hr>';
								
								// require_once 'reset_activation.php';
								
								break;
							case 'generate' :
								
								db_runProcedure();
								getNewKeysForToday();
								break;
							case 'newkeys' :
								getNewKeysForToday();
								break;
						}
					}
					
					if (isset ( $_POST ['Reset'] )) {
						
						ResetActivation ( $_POST ['activated_id'] );
					}
				}
				
				?>	  	
	  	
	  </div>

	</div>

	<!--  Форма -->



	</div>





	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

	<!-- Include all compiled plugins (below), or include individual files as needed -->

	<script src="/app/frontend/web/lib/bootstrap337/js/bootstrap.min.js"></script>

</body>

</html>

