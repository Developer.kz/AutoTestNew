--select count(*) from question q;
select count(*) from question q;

--all questions
SELECT q.id qid, q.num, q.name_ru question, a.id answer_id, a.name_ru answer, ra.id rid, ra.answer_id right_answer_id, s.name_ru, ar.id article_id, ar.name_ru, ar.doc, p.id pid 
from question q
--SELECT ra.* from question q
	left join answer a on a.question_id = q.Id
	left join right_answer ra on ra.answer_id = a.Id
	left join article ar on ar.Id = ra.article_id
	left join "section" s on s.Id = q.section_id
	left join picture p on p.question_id = q.id
order by q.id

--specific question
SELECT  q.id qid, q.num, q.name_ru question, 
		a.id answer_id, a.name_ru answer, 
		ra.id rid, ra.answer_id right_answer_id, 
		s.name_ru 'section', s.num 'section_num', 
		ar.id article_id, ar.name_ru article, ar.doc, 
		p.id pid 
from question q
	left join answer a on a.question_id = q.Id
	left join right_answer ra on ra.answer_id = a.Id
	left join article ar on ar.Id = ra.article_id
	left join "section" s on s.Id = q.section_id
	left join picture p on p.question_id = q.id
where 
	--s.num = 29
	q.num like '29%'
	--q.id = 61
	and ar.doc = 'expl.html'
order by answer_id;

update right_answer set answer_id = 1969 where Id = 576;

select * from answer where id = 623;
update answer set name_ru = '2. Не ограничивает обзор водителям других транспортных средств.' where id = 1880;

select * from question WHERE id = 280;
update question set name_ru = 'Превышение водителем транспортного средства установленной скорости движения более 20 км/час, совершенное повторно в течение года после наложения административного взыскания, влечет:' where id = 868;

update article set name_ru = '19.1, 7.1.5 ' where id = 494; 
update article set doc = 'per.html' where id = 766;

delete from question where id = 535;
delete from answer where id in (1839, 1840, 1841);
delete from right_answer where id = 535;
delete from article where id = 535;

--отчёт
SELECT q.num '№ вопроса', q.name_ru 'вопрос', 
	   a.name_ru 'вариант ответа', ra.answer_id 'правильный', 
	   s.name_ru 'секция', 
	   ar.name_ru 'пункт в документе', ar.doc 'документ' 
from question q
--SELECT ra.* from question q
	left join answer a on a.question_id = q.Id
	left join right_answer ra on ra.answer_id = a.Id
	left join article ar on ar.Id = ra.article_id
	left join "section" s on s.Id = q.section_id
order by q.id asc;

-- разбор багов
select * from answer a where 
	a.name_ru like '%демпфер%'
	a.question_id = 61;
select * from question q where q.id = 61;
--61	2.26	2	Рулевое управление считается недействующим, если:
INSERT INTO question (Id, num, section_id, name_ru) VALUES(61, '2.26', 2, 'Рулевое управление считается недействующим, если:');

select * from question q order by q.id;

--последние корректировки по 29.1-29.18
--specific question
select * from article ar where id in (738,739,740,741,742,743,744,745,746,747,748,749,750,751,752,753,754,755);
update article set doc = 'per.html' where id in (738,739,740,741,742,743,744,745,746,747,748,749,750,751,752,753,754,755);
commit;
