﻿Imports System.Net
Public Class FrmActivate

    Private product_id As String = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\MICROSOFT\WINDOWS NT\CURRENTVERSION", "ProductID", "")
    Private Declare Function InternetGetConnectedState Lib "wininet.dll" (ByRef lpdwFlags As Int32, ByVal dwReserved As Int32) As Boolean

    Private Sub MaterialRaisedButton2_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs)
        FrmReg.Show()
    End Sub

    Public Function EncodeBase64(input As String) As String
        Return System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(input))
    End Function
    Private Function ToUnicode(ByVal str As String) As String
        Dim utf8Encoding As New System.Text.UTF8Encoding
        Dim encodedString() As Byte
        encodedString = utf8Encoding.GetBytes(str)
        Return utf8Encoding.GetString(encodedString)
    End Function
    Public Function ActivateRegistration(ByVal product_key As String, ByVal firstname As String, ByVal lastname As String, ByVal email As String) As Boolean
        Dim b As Boolean
        b = False
        Dim Uri As String
        'Uri = "http://localhost/postsernum.php"
        Uri = "http://www.lem.kz/postsernum.php"
        Dim myParameters As String
        myParameters = "product_key=" & Trim(product_key) & "&product_id=" & Trim(product_id) & "&firstname=" & EncodeBase64(Trim(firstname)) & "&lastname=" & EncodeBase64(Trim(lastname)) & "&email=" & Trim(email)
        'MsgBox(HttpContext.Current.Server.UrlEncode(myParameters))
        Using wc As New WebClient()
            wc.Headers(HttpRequestHeader.ContentType) = "application/x-www-form-urlencoded"
            'Dim HtmlResult As String = wc.UploadString(Uri, myParameters)
            Dim HtmlResult As String = wc.DownloadString(Uri & "?" & myParameters)
            If HtmlResult = "Activated" Then
                Module1.FixActivation(product_key, product_id, firstname, lastname, email)
                MsgBox("Ваш продукт активирован.")
                b = True
            Else
                If HtmlResult = "Maximum" Then
                    MsgBox("Извините, но Вы использовали максимальное количество активаций.")
                Else
                    'MsgBox("Ошибка активации, ответ с сервера активации - " & HtmlResult)
                    If HtmlResult = "Product key not exist" Then
                        MsgBox("Введён несуществующий серийный номер " & product_key)
                    Else
                        MsgBox("Ошибка активации, ответ с сервера активации - " & HtmlResult)
                    End If
                End If
            End If
        End Using
        Return b
    End Function
    Private Function CheckInternet() As Boolean
        Return InternetGetConnectedState(0, 0&)
    End Function
    Private Sub FrmActivate_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        If Module1.isProd Then
            If Module1.CheckActivation(product_id) Then
                Me.Hide()
                FrmMain.Show()
            End If
        Else
            Me.Hide()
            FrmMain.Show()
        End If
    End Sub
    Private Sub CheckActivation()
        If Module1.CheckActivation(product_id) Then
            Me.Hide()
            FrmMain.Show()
        End If
    End Sub

    Private Sub BtnActivateNew_Click(sender As Object, e As EventArgs) Handles BtnActivateNew.Click
    End Sub

    Private Sub BtnActivateNew_MouseDown(sender As Object, e As MouseEventArgs) Handles BtnActivateNew.MouseDown
        BtnActivateNew.BackgroundImage = Image.FromFile(picpath + "BtnActivatePressed.jpg")
    End Sub

    Private Sub BtnActivateNew_MouseUp(sender As Object, e As MouseEventArgs) Handles BtnActivateNew.MouseUp
        BtnActivateNew.BackgroundImage = Image.FromFile(picpath + "BtnActivate.jpg")
        If CheckInternet() Then
            FrmReg.Show()
        Else
            MsgBox("Проверьте соединение с интернетом!")
        End If
    End Sub

    Private Sub BtnCancelNew_MouseDown(sender As Object, e As MouseEventArgs) Handles BtnCancelNew.MouseDown
        BtnCancelNew.BackgroundImage = Image.FromFile(picpath + "BtnCancelPressed.jpg")
    End Sub

    Private Sub BtnCancelNew_MouseUp(sender As Object, e As MouseEventArgs) Handles BtnCancelNew.MouseUp
        BtnCancelNew.BackgroundImage = Image.FromFile(picpath + "BtnCancel.jpg")
        Me.Close()
    End Sub

    Private Sub FrmActivate_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Module1.initApp()
        Me.BackgroundImage = Image.FromFile(picpath + "splash.jpg")
    End Sub
End Class
