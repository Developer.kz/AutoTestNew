﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.MaterialRaisedButton1 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.BtnTestNew = New System.Windows.Forms.Button()
        Me.BtnExamNew = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'MaterialRaisedButton1
        '
        Me.MaterialRaisedButton1.Depth = 0
        Me.MaterialRaisedButton1.Location = New System.Drawing.Point(845, 624)
        Me.MaterialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton1.Name = "MaterialRaisedButton1"
        Me.MaterialRaisedButton1.Primary = True
        Me.MaterialRaisedButton1.Size = New System.Drawing.Size(151, 42)
        Me.MaterialRaisedButton1.TabIndex = 5
        Me.MaterialRaisedButton1.Text = "Импорт"
        Me.MaterialRaisedButton1.UseVisualStyleBackColor = True
        Me.MaterialRaisedButton1.Visible = False
        '
        'BtnTestNew
        '
        Me.BtnTestNew.BackgroundImage = CType(resources.GetObject("BtnTestNew.BackgroundImage"), System.Drawing.Image)
        Me.BtnTestNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnTestNew.Location = New System.Drawing.Point(721, 509)
        Me.BtnTestNew.Name = "BtnTestNew"
        Me.BtnTestNew.Size = New System.Drawing.Size(181, 67)
        Me.BtnTestNew.TabIndex = 6
        Me.BtnTestNew.UseVisualStyleBackColor = True
        '
        'BtnExamNew
        '
        Me.BtnExamNew.BackgroundImage = CType(resources.GetObject("BtnExamNew.BackgroundImage"), System.Drawing.Image)
        Me.BtnExamNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnExamNew.Location = New System.Drawing.Point(413, 509)
        Me.BtnExamNew.Name = "BtnExamNew"
        Me.BtnExamNew.Size = New System.Drawing.Size(181, 67)
        Me.BtnExamNew.TabIndex = 7
        Me.BtnExamNew.UseVisualStyleBackColor = True
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.BtnExamNew)
        Me.Controls.Add(Me.BtnTestNew)
        Me.Controls.Add(Me.MaterialRaisedButton1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Главное меню"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents MaterialRaisedButton1 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents BtnTestNew As Button
    Friend WithEvents BtnExamNew As Button
End Class
