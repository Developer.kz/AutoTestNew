﻿Imports System.IO
Imports Excel = Microsoft.Office.Interop.Excel

Public Class FrmImport
    Private Sub FrmImport_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Application.Exit()
    End Sub

    Private Sub FrmImport_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        Dim book As Excel.Workbook
        Dim oSheet1 As Object
        Dim filename As String = "C:\Projects\AutoTestNew\Materials\ТаблицаДляПрограммы1.xlsx"

        Dim excel As Microsoft.Office.Interop.Excel.Application
        excel = New Microsoft.Office.Interop.Excel.Application
        excel.Visible = False

        book = excel.Workbooks.Open(filename, ReadOnly:=False)
        excel.Visible = False

        oSheet1 = book.Worksheets(1)

        Dim razdel As String
        Dim prevrazdel As Integer
        prevrazdel = 0
        Dim qnum As String
        Dim prevqnum As String
        Dim pic As String
        Dim qname As String
        Dim answer As String
        Dim article As String
        Dim isright As Boolean
        Dim query As String
        Dim quest_id As Integer
        Dim html As String
        Dim pos As Integer
        Dim doc As String
        Dim counter As Integer

        quest_id = 0
        prevqnum = ""
        counter = 0
        'очистка всех таблиц для вопросов
        Module1.Truncate()

        For i = 2 To 3028
            razdel = ""
            doc = ""
            qnum = ""
            pic = ""
            qname = ""
            answer = ""
            article = ""
            isright = False
            For j = 1 To 7
                If j = 1 Then
                    If oSheet1.Cells(i, j).Value IsNot Nothing Then
                        razdel = oSheet1.Cells(i, j).Value
                        prevrazdel = razdel
                    Else
                        razdel = prevrazdel
                    End If
                End If
                If j = 2 Then
                    If oSheet1.Cells(i, j).Value IsNot Nothing Then
                        qnum = oSheet1.Cells(i, j).Value
                        prevqnum = qnum
                    Else
                        qnum = prevqnum
                    End If
                End If
                If j = 3 Then
                    If oSheet1.Cells(i, j).Value IsNot Nothing Then
                        pic = oSheet1.Cells(i, j).Value
                        'pic = pic.Replace("jpeg", "jpg")
                        pic = pic.Trim()
                        pic = "C:\Projects\AutoTestNew\Materials\images1\раздел " + razdel + "\" + pic
                    End If
                End If
                If j = 4 Then
                    If oSheet1.Cells(i, j).Value IsNot Nothing Then
                        qname = oSheet1.Cells(i, j).Value
                    End If
                End If
                If j = 5 Then
                    If oSheet1.Cells(i, j).Value IsNot Nothing Then
                        answer = oSheet1.Cells(i, j).Value
                    End If
                End If
                If j = 6 Then
                    If oSheet1.Cells(i, j).Value = 1 Then
                        isright = True
                        article = oSheet1.Cells(i, j + 1).Value
                        If String.IsNullOrEmpty(article) Then
                            article = ""
                        Else
                            article = article.Trim()
                        End If
                        html = oSheet1.Cells(i, j + 2).Value
                        If String.IsNullOrEmpty(html) = False Then
                            html = html.Trim()
                        End If
                        pos = InStr(html, "движения")
                        If pos > 0 Then
                            doc = "pdd.html"
                        End If
                        pos = InStr(html, "Дорожные")
                        If pos > 0 Then
                            doc = "znaki.html"
                            If article = "" Then
                                article = "Приложение 1 к ПДД ""Дорожные знаки"""
                            End If
                        End If
                        pos = InStr(html, "эксплуатации")
                        If pos > 0 Then
                            doc = "expl.html"
                        End If
                        pos = InStr(html, "ПЕРЕЧЕНЬ")
                        If pos > 0 Then
                            doc = "per.html"
                        End If
                        pos = InStr(html, "Административных")
                        If pos > 0 Then
                            doc = "adm.html"
                        End If
                        pos = InStr(html, "Основы")
                        If pos > 0 Then
                            doc = "osnovy.html"
                            article = "Основы безопасности дорожного движения"
                        End If
                        pos = InStr(html, "медицинской")
                        If pos > 0 Then
                            doc = "med.html"
                            article = "Оказание первой медицинской помощи"
                        End If
                    End If
                End If
            Next j
            'ListBox1.Items.Add("Раздел: " + razdel.ToString + ", № вопроса: " + qnum.ToString + ", картинка: " + pic + ", вопрос: " + qname + ", вариант: " + answer + "Правильный: " + isright.ToString + ", подсказка: " + article)

            If qname <> "" Then
                counter += 1
                ListBox1.Items.Add(CStr(counter) + ") -------------------------------------------------------")
                Me.Text = CStr(counter)
                query = "insert into question (num, section_id, name_ru) values (" + qnum + ", " + razdel + ", '" + qname + "');"
                Module1.ExecuteQuery(query)
                quest_id = Module1.GetLastInsertedID("question")
                ListBox1.Items.Add("question = " + qname + ", id = " + quest_id.ToString)
            End If
            Dim answer_id As Integer
            answer_id = 0
            If answer <> "" Then
                query = "insert into answer (name_ru, question_id) values ('" + answer + "', " + quest_id.ToString + ");"
                Module1.ExecuteQuery(query)
                answer_id = Module1.GetLastInsertedID("answer")
                ListBox1.Items.Add("answer = " + answer + ", id = " + answer_id.ToString + ", quest_id = " + quest_id.ToString)
            End If
            If pic <> "" Then
                InsertPicture(pic, quest_id)
                ListBox1.Items.Add("pic = " + pic + ", quest_id = " + quest_id.ToString)
            End If
            Dim article_id As Integer
            article_id = 0
            If isright = True Then
                query = "insert into article (name_ru, doc) values('" + article + "', '" + doc + "');"
                Module1.ExecuteQuery(query)
                article_id = Module1.GetLastInsertedID("article")
                ListBox1.Items.Add("article = " + article + ", id = " + article_id.ToString + ", doc = " + doc)
            End If
            If isright = True Then
                query = "insert into right_answer (answer_id, article_id) values (" + answer_id.ToString + ", " + article_id.ToString + ");"
                Module1.ExecuteQuery(query)
                ListBox1.Items.Add("right_answer, answer_id = " + answer_id.ToString + ", article_id = " + article_id.ToString)
            End If
        Next i
        book.Close()
        book = Nothing
        excel.Quit()
        MsgBox("Import finished.")
    End Sub

    Private Sub FrmImport_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub
End Class