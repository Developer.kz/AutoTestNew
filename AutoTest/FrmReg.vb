﻿
Public Class FrmReg
    Private Sub MaterialRaisedButton2_Click(sender As Object, e As EventArgs)
        Close()
    End Sub

    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs)
        If (String.IsNullOrEmpty(InpNum.Text)) Or (String.IsNullOrEmpty(InpName.Text)) Or (String.IsNullOrEmpty(InpLastname.Text)) Or (String.IsNullOrEmpty(InpEmail.Text)) Then
            MsgBox("Необходимо заполнить все поля!")
        Else
            If FrmActivate.ActivateRegistration(InpNum.Text, InpName.Text, InpLastname.Text, InpEmail.Text) Then
                Me.Hide()
                FrmMain.Show()
            Else
                MsgBox("Попробуйте ввести другие данные.")
            End If
        End If
    End Sub

    Private Sub BtnActivateNew_MouseDown(sender As Object, e As MouseEventArgs) Handles BtnActivateNew.MouseDown
        BtnActivateNew.BackgroundImage = Image.FromFile(picpath + "BtnRegPressed.jpg")
    End Sub

    Private Sub BtnActivateNew_MouseUp(sender As Object, e As MouseEventArgs) Handles BtnActivateNew.MouseUp
        BtnActivateNew.BackgroundImage = Image.FromFile(picpath + "BtnReg.jpg")
        If (String.IsNullOrEmpty(InpNum.Text)) Or (String.IsNullOrEmpty(InpName.Text)) Or (String.IsNullOrEmpty(InpLastname.Text)) Or (String.IsNullOrEmpty(InpEmail.Text)) Then
            MsgBox("Необходимо заполнить все поля!")
        Else
            If FrmActivate.ActivateRegistration(InpNum.Text, InpName.Text, InpLastname.Text, InpEmail.Text) Then
                Me.Hide()
                FrmMain.Show()
            Else
                MsgBox("Попробуйте ввести другие данные.")
            End If
        End If
    End Sub

    Private Sub BtnCancelNew_MouseDown(sender As Object, e As MouseEventArgs) Handles BtnCancelNew.MouseDown
        BtnCancelNew.BackgroundImage = Image.FromFile(picpath + "BtnCancelPressed.jpg")
    End Sub

    Private Sub BtnCancelNew_MouseUp(sender As Object, e As MouseEventArgs) Handles BtnCancelNew.MouseUp
        BtnCancelNew.BackgroundImage = Image.FromFile(picpath + "BtnCancel.jpg")
        Close()
    End Sub
End Class