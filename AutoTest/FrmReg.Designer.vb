﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReg
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmReg))
        Me.LblFirstName = New MaterialSkin.Controls.MaterialLabel()
        Me.LblLastName = New MaterialSkin.Controls.MaterialLabel()
        Me.LblEmail = New MaterialSkin.Controls.MaterialLabel()
        Me.InpName = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.InpLastname = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.InpEmail = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.LblText = New MaterialSkin.Controls.MaterialLabel()
        Me.InpNum = New MaterialSkin.Controls.MaterialSingleLineTextField()
        Me.LblNum = New MaterialSkin.Controls.MaterialLabel()
        Me.BtnActivateNew = New System.Windows.Forms.Button()
        Me.BtnCancelNew = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LblFirstName
        '
        Me.LblFirstName.AutoSize = True
        Me.LblFirstName.Depth = 0
        Me.LblFirstName.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.LblFirstName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LblFirstName.Location = New System.Drawing.Point(29, 49)
        Me.LblFirstName.MouseState = MaterialSkin.MouseState.HOVER
        Me.LblFirstName.Name = "LblFirstName"
        Me.LblFirstName.Size = New System.Drawing.Size(87, 19)
        Me.LblFirstName.TabIndex = 0
        Me.LblFirstName.Text = "ФАМИЛИЯ"
        '
        'LblLastName
        '
        Me.LblLastName.AutoSize = True
        Me.LblLastName.Depth = 0
        Me.LblLastName.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.LblLastName.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LblLastName.Location = New System.Drawing.Point(29, 87)
        Me.LblLastName.MouseState = MaterialSkin.MouseState.HOVER
        Me.LblLastName.Name = "LblLastName"
        Me.LblLastName.Size = New System.Drawing.Size(43, 19)
        Me.LblLastName.TabIndex = 1
        Me.LblLastName.Text = "ИМЯ"
        '
        'LblEmail
        '
        Me.LblEmail.AutoSize = True
        Me.LblEmail.Depth = 0
        Me.LblEmail.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.LblEmail.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LblEmail.Location = New System.Drawing.Point(29, 128)
        Me.LblEmail.MouseState = MaterialSkin.MouseState.HOVER
        Me.LblEmail.Name = "LblEmail"
        Me.LblEmail.Size = New System.Drawing.Size(51, 19)
        Me.LblEmail.TabIndex = 2
        Me.LblEmail.Text = "E-Mail"
        '
        'InpName
        '
        Me.InpName.Depth = 0
        Me.InpName.Hint = ""
        Me.InpName.Location = New System.Drawing.Point(143, 49)
        Me.InpName.MouseState = MaterialSkin.MouseState.HOVER
        Me.InpName.Name = "InpName"
        Me.InpName.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.InpName.SelectedText = ""
        Me.InpName.SelectionLength = 0
        Me.InpName.SelectionStart = 0
        Me.InpName.Size = New System.Drawing.Size(286, 23)
        Me.InpName.TabIndex = 5
        Me.InpName.UseSystemPasswordChar = False
        '
        'InpLastname
        '
        Me.InpLastname.Depth = 0
        Me.InpLastname.Hint = ""
        Me.InpLastname.Location = New System.Drawing.Point(143, 87)
        Me.InpLastname.MouseState = MaterialSkin.MouseState.HOVER
        Me.InpLastname.Name = "InpLastname"
        Me.InpLastname.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.InpLastname.SelectedText = ""
        Me.InpLastname.SelectionLength = 0
        Me.InpLastname.SelectionStart = 0
        Me.InpLastname.Size = New System.Drawing.Size(286, 23)
        Me.InpLastname.TabIndex = 6
        Me.InpLastname.UseSystemPasswordChar = False
        '
        'InpEmail
        '
        Me.InpEmail.Depth = 0
        Me.InpEmail.Hint = ""
        Me.InpEmail.Location = New System.Drawing.Point(143, 124)
        Me.InpEmail.MouseState = MaterialSkin.MouseState.HOVER
        Me.InpEmail.Name = "InpEmail"
        Me.InpEmail.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.InpEmail.SelectedText = ""
        Me.InpEmail.SelectionLength = 0
        Me.InpEmail.SelectionStart = 0
        Me.InpEmail.Size = New System.Drawing.Size(286, 23)
        Me.InpEmail.TabIndex = 7
        Me.InpEmail.UseSystemPasswordChar = False
        '
        'LblText
        '
        Me.LblText.AutoSize = True
        Me.LblText.Depth = 0
        Me.LblText.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.LblText.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LblText.Location = New System.Drawing.Point(29, 159)
        Me.LblText.MouseState = MaterialSkin.MouseState.HOVER
        Me.LblText.Name = "LblText"
        Me.LblText.Size = New System.Drawing.Size(471, 19)
        Me.LblText.TabIndex = 8
        Me.LblText.Text = "Введите восьмизначный код активации CD например, A57E5190"
        '
        'InpNum
        '
        Me.InpNum.Depth = 0
        Me.InpNum.Hint = ""
        Me.InpNum.Location = New System.Drawing.Point(143, 185)
        Me.InpNum.MouseState = MaterialSkin.MouseState.HOVER
        Me.InpNum.Name = "InpNum"
        Me.InpNum.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
        Me.InpNum.SelectedText = ""
        Me.InpNum.SelectionLength = 0
        Me.InpNum.SelectionStart = 0
        Me.InpNum.Size = New System.Drawing.Size(286, 23)
        Me.InpNum.TabIndex = 10
        Me.InpNum.UseSystemPasswordChar = False
        '
        'LblNum
        '
        Me.LblNum.AutoSize = True
        Me.LblNum.Depth = 0
        Me.LblNum.Font = New System.Drawing.Font("Roboto", 11.0!)
        Me.LblNum.ForeColor = System.Drawing.Color.FromArgb(CType(CType(222, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.LblNum.Location = New System.Drawing.Point(29, 189)
        Me.LblNum.MouseState = MaterialSkin.MouseState.HOVER
        Me.LblNum.Name = "LblNum"
        Me.LblNum.Size = New System.Drawing.Size(40, 19)
        Me.LblNum.TabIndex = 9
        Me.LblNum.Text = "КОД"
        '
        'BtnActivateNew
        '
        Me.BtnActivateNew.BackgroundImage = CType(resources.GetObject("BtnActivateNew.BackgroundImage"), System.Drawing.Image)
        Me.BtnActivateNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnActivateNew.Location = New System.Drawing.Point(237, 257)
        Me.BtnActivateNew.Name = "BtnActivateNew"
        Me.BtnActivateNew.Size = New System.Drawing.Size(181, 67)
        Me.BtnActivateNew.TabIndex = 12
        Me.BtnActivateNew.UseVisualStyleBackColor = True
        '
        'BtnCancelNew
        '
        Me.BtnCancelNew.BackgroundImage = CType(resources.GetObject("BtnCancelNew.BackgroundImage"), System.Drawing.Image)
        Me.BtnCancelNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnCancelNew.Location = New System.Drawing.Point(445, 257)
        Me.BtnCancelNew.Name = "BtnCancelNew"
        Me.BtnCancelNew.Size = New System.Drawing.Size(181, 67)
        Me.BtnCancelNew.TabIndex = 11
        Me.BtnCancelNew.UseVisualStyleBackColor = True
        '
        'FrmReg
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(638, 352)
        Me.Controls.Add(Me.BtnActivateNew)
        Me.Controls.Add(Me.BtnCancelNew)
        Me.Controls.Add(Me.InpNum)
        Me.Controls.Add(Me.LblNum)
        Me.Controls.Add(Me.LblText)
        Me.Controls.Add(Me.InpEmail)
        Me.Controls.Add(Me.InpLastname)
        Me.Controls.Add(Me.InpName)
        Me.Controls.Add(Me.LblEmail)
        Me.Controls.Add(Me.LblLastName)
        Me.Controls.Add(Me.LblFirstName)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "FrmReg"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ввод регистрационных данных"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblFirstName As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents LblLastName As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents LblEmail As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents InpName As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents InpLastname As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents InpEmail As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents LblText As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents InpNum As MaterialSkin.Controls.MaterialSingleLineTextField
    Friend WithEvents LblNum As MaterialSkin.Controls.MaterialLabel
    Friend WithEvents BtnActivateNew As Button
    Friend WithEvents BtnCancelNew As Button
End Class
