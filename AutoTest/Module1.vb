﻿Imports System.Data.SQLite
Imports System.IO
Module Module1

    Public isProd As Boolean = False
    Private dbpath As String
    Public picpath As String
    Public htmlpath As String
    Private conn As SQLiteConnection
    Public quesArr() As Integer
    Public curQuesId As Integer
    Public curQuesIndex As Integer
    Public answersArr() As String
    Public answersIdArr() As Integer
    Public totalRightAnswers As Integer
    Public isExam As Boolean 'режим теста, true - экзамен, false - пробный тест
    Public Const testMaxQues As Long = 40
    Public totalQuesInDb As Long
    Public htmlFile As String
    Private userProfileFolder = System.Environment.ExpandEnvironmentVariables("%USERPROFILE%")
    Private logFile = userProfileFolder + "\lem.log"

    Public Sub initApp()
        If isProd.Equals(True) Then
            dbpath = "URI=file:" & Application.StartupPath & "\db\autotest_db.db;"
            picpath = Application.StartupPath & "\pic\"
            htmlpath = Application.StartupPath & "\html\"
        Else
            dbpath = "URI=file:c:\dev\prj\lem\AutoTestNew\autotest_db.db;"
            picpath = "c:\dev\prj\lem\AutoTestNew\AutoTest\pic\"
            htmlpath = "c:\dev\prj\lem\AutoTestNew\AutoTest\html\"
        End If
        conn = New SQLiteConnection(dbpath)
        OpenConn()
        totalQuesInDb = getTotalQuesDb()
    End Sub

    Public Sub ReDimArrQuestions(max As Integer)
        ReDim quesArr(testMaxQues)
        InitArrQuestions()
    End Sub

    Private Sub InitArrQuestions()
        Dim RandomClass As New Random()
        Dim RememberSet As New HashSet(Of Integer)

        Dim RandomNumber As Integer
        Dim i As Integer
        i = 1
        While RememberSet.Count < testMaxQues
            RandomNumber = RandomClass.Next(1, totalQuesInDb)
            RandomNumber = RandomNumber + 1
            If RememberSet.Add(RandomNumber) Then
                i = i + 1
            End If
        End While
        i = 0
        For Each item As Integer In RememberSet
            quesArr(i) = item
            i = i + 1
        Next
        Array.Resize(quesArr, quesArr.GetUpperBound(0))
    End Sub
    Public Sub OpenConn() 'Открытие соединения с БД
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If
        Catch ex As Exception
            MsgBox("Ошибка открытия файла базы данных по пути " + dbpath)
            Application.Exit()

        End Try
    End Sub

    Public Sub CloseConn() 'Закрытие соединения с БД
        Try
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        Catch ex As Exception
            MsgBox("Ошибка закрытия файла базы данных по пути " + dbpath)
        End Try
    End Sub

    Public Sub GetAnswers()
        Dim ind As Integer
        Dim answers_count As Integer
        Dim id As Integer = quesArr(curQuesIndex)
        answers_count = 0
        ind = 0
        Try
            Dim cmd_count As SQLiteCommand = New SQLiteCommand("Select count(name_ru) As k from answer where question_id = " & id, conn)
            Dim reader_count As SQLiteDataReader = cmd_count.ExecuteReader
            While (reader_count.Read())
                answers_count = reader_count("k")
            End While
            'MsgBox(Convert.ToString(answers_count))
            ReDim answersArr(answers_count)
            ReDim answersIdArr(answers_count)
            Dim cmd As SQLiteCommand = New SQLiteCommand("Select id, name_ru from answer where question_id = " & id, conn)
            Dim reader As SQLiteDataReader = cmd.ExecuteReader
            While (reader.Read())
                ind = ind + 1
                answersArr(ind) = reader("name_ru")
                answersIdArr(ind) = CInt(reader("id"))
            End While
        Catch ex As Exception
            MsgBox("Ошибка чтения ответа из БД под номером " & id)
        End Try
    End Sub

    Public Function GetPicFromDb(questionId As Integer) As MemoryStream
        Dim pic As MemoryStream
        Try
            Dim cmd As SQLiteCommand = New SQLiteCommand("Select picture FROM picture WHERE question_id = " & quesArr(questionId), conn)
            Dim data As Byte() = cmd.ExecuteScalar()
            Dim stmBLOBData As New MemoryStream(data)
            pic = stmBLOBData
            'PictureBox1.Image = Image.FromStream(stmBLOBData)
        Catch ex As Exception
            MsgBox("Ошибка чтения рисунка из БД под номером " + quesArr(questionId))
        End Try
        Return pic
    End Function

    Public Function IsRightAnswer(questionId As Integer, answeredId As Integer) As Boolean
        Dim right As Boolean
        right = False
        Try
            Dim cmd As SQLiteCommand = New SQLiteCommand("
            select count(right_answer.id) as k from right_answer
            join question on question.Id = " & quesArr(questionId) & "
            join answer on answer.question_id = question.id
            where right_answer.answer_Id = " & answeredId, conn)
            Dim reader As SQLiteDataReader = cmd.ExecuteReader
            While (reader.Read())
                If reader("k") = 1 Then right = True
            End While
        Catch ex As Exception
            MsgBox("Ошибка чтения правильного ответа из БД ID " & questionId)
        End Try
        Return right
    End Function

    Public Function GetArticle(questionId As Integer) As String
        Dim article As String
        article = ""
        Try
            Dim cmd As SQLiteCommand = New SQLiteCommand("
            select article.name_ru as 'article', doc from right_answer
            join question on question.Id = " & quesArr(questionId) & "
            join answer on answer.question_id = question.id
            join article on article.id = right_answer.article_id
            where right_answer.answer_Id = answer.id", conn)
            Dim reader As SQLiteDataReader = cmd.ExecuteReader
            While (reader.Read())
                article = reader("article")
                htmlFile = reader("doc")
            End While
        Catch ex As Exception
            MsgBox("Ошибка чтения правильного ответа из БД ID" & questionId)
        End Try
        Return article
    End Function

    Public Function GetRightAnswerId(questionId As Integer) As Integer
        Dim id As Integer
        id = 0
        Try
            Dim cmd As SQLiteCommand = New SQLiteCommand("
            select right_answer.answer_Id as 'id' from right_answer
            join question on question.Id = " & quesArr(questionId) & "
            join answer on answer.question_id = question.id
            where right_answer.answer_Id = answer.id", conn)
            Dim reader As SQLiteDataReader = cmd.ExecuteReader
            While (reader.Read())
                id = reader("id")
            End While
        Catch ex As Exception
            Dim msg = "Ошибка чтения ID правильного ответа из БД ID " & questionId
            log(msg)
        End Try
        Return id
    End Function
    'Фиксирование записи о активации в локальной базе данных
    Public Sub FixActivation(product_key As String, product_id As String, firstname As String, lastname As String, email As String)
        OpenConn()
        Try
            Dim query As String
            Dim localDate = DateTime.Now
            query = "insert into product (product_key, product_id, value, max, modified, firstname, lastname, email) values ('" & product_key & "', '" & product_id & "', 1, 1, '" & localDate.ToString & "', '" & firstname & "', '" & lastname & "', '" & email & "')"
            Dim cmd As SQLiteCommand = New SQLiteCommand(query, conn)
            Try
                'Активация локально зафикирована
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                MessageBox.Show("Ошибка записи активации в лок БД. " & ex.Message, "Insert Records")
            End Try
        Catch ex As Exception
            MsgBox("Ошибка локальной фиксации активации ПО!" & ex.Message)
        End Try
        CloseConn()
    End Sub
    'проверка в локальной базе записи активации
    Public Function CheckActivation(product_id As String) As Boolean
        Dim k As Integer
        k = 0
        Try
            OpenConn()
            Dim cmd As SQLiteCommand = New SQLiteCommand("select count(id) as k from product where product_id = '" & product_id & "'", conn)
            Dim reader As SQLiteDataReader = cmd.ExecuteReader
            While (reader.Read())
                k = reader("k")
            End While
            CloseConn()
        Catch ex As Exception
            MsgBox("CheckActivation: Ошибка чтения информации об активации в БД. " + ex.Message.ToString)
            Return False
        End Try
        If k <> 0 Then
            Return True
        Else Return False
        End If
    End Function
    Public Sub ExecuteQuery(ByVal query As String)
        Try
            Dim cmd As SQLiteCommand = New SQLiteCommand(query, conn)
            Try
                cmd.ExecuteNonQuery()
            Catch ex As Exception
                MessageBox.Show("Ошибка записи вопроса в лок БД. " & ex.Message, "Insert Records")
            End Try
        Catch ex As Exception
            MsgBox("Ошибка лок БД" & ex.Message)
        End Try
    End Sub
    Public Function GetLastInsertedID(ByVal table As String) As Integer
        Dim id As Integer
        id = 0
        Try
            Dim cmd As SQLiteCommand = New SQLiteCommand("SELECT MAX(id) as id FROM " + table, conn)
            'Dim cmd As SQLiteCommand = New SQLiteCommand("SELECT seq as id from sqlite_sequence WHERE lower(name)='question';", conn)
            Dim reader As SQLiteDataReader = cmd.ExecuteReader
            While (reader.Read())
                id = reader("id")
            End While
        Catch ex As Exception
            MsgBox("GetLastInsertedID: Ошибка чтения информации об активации в БД")
        End Try
        Return id
    End Function
    Public Sub InsertPicture(ByVal imagePath As String, ByVal question_id As Integer)
        Dim data As Byte()
        Try
            data = File.ReadAllBytes(imagePath)
        Catch ex As Exception
            Console.WriteLine(ex.ToString())
        End Try
        Dim cmd As New SQLiteCommand(conn)
        cmd.CommandText = "INSERT INTO picture (picture, question_id) VALUES (@img, " + question_id.ToString + ")"
        cmd.Prepare()
        cmd.Parameters.Add("@img", DbType.Binary, data.Length)
        cmd.Parameters("@img").Value = data
        cmd.ExecuteNonQuery()
        'CloseConn()
    End Sub
    Public Sub Truncate()
        ExecuteQuery("delete from answer;")
        ExecuteQuery("delete from article;")
        ExecuteQuery("delete from picture;")
        ExecuteQuery("delete from question;")
        ExecuteQuery("delete from right_answer;")
    End Sub
    Public Function PicExistsQuestion(ByVal id As Integer) As Boolean
        Dim k As Integer
        k = 0
        Try
            Dim cmd As SQLiteCommand = New SQLiteCommand("SELECT count(*) as k FROM picture WHERE question_id = " & quesArr(id), conn)
            Dim reader As SQLiteDataReader = cmd.ExecuteReader
            While (reader.Read())
                k = reader("k")
            End While
        Catch ex As Exception
            MsgBox("Ошибка чтения рисунка из БД под номером " + quesArr(id))
        End Try
        'CloseConn()
        If k = 0 Then
            FrmTest.PicBox.Image = Nothing
            Return False
        Else Return True
        End If
    End Function
    Public Function getTotalQuesDb() As Integer
        Dim k As Integer
        k = 0
        Try
            Dim cmd As SQLiteCommand = New SQLiteCommand("SELECT count(*) as k FROM question", conn)
            Dim reader As SQLiteDataReader = cmd.ExecuteReader
            While (reader.Read())
                k = reader("k")
            End While
        Catch ex As Exception
            MsgBox("Ошибка получения количества вопросов")
        End Try
        Return k
    End Function
    Public Sub log(msg As String)
        logToFile(msg, logFile)
    End Sub
    Private Sub logToFile(msg As String, filePath As String)
        Dim file As System.IO.StreamWriter
        file = My.Computer.FileSystem.OpenTextFileWriter(filePath, True)
        file.WriteLine(msg)
        file.Close()
    End Sub
    Public Sub log()
        log("Question array length " & quesArr.Length)
        Dim i As Integer
        For i = 0 To quesArr.Length - 1
            log("arr[" & i & "] = " & quesArr(i))
        Next
    End Sub

    Public Sub logCurQuesNum()
        log("Current question number - " + Conversion.Str(curQuesId))
    End Sub

    Public Sub questionForward()
        curQuesIndex += 1
    End Sub
    Public Function getCurrentQuestion() As String
        Dim questionText As String = String.Empty
        Dim id As Integer = 0
        Try
            id = quesArr(curQuesIndex)
        Catch ex As Exception
            Dim msg As String = "Error, can't get question from questions array at index - " & curQuesIndex & " ID " & id
            log(msg)
            log()
            MsgBox(msg)
            Return String.Empty
        End Try
        Try
            Dim cmd As SQLiteCommand = New SQLiteCommand("Select name_ru from question where id = " & id, conn)
            Dim reader As SQLiteDataReader = cmd.ExecuteReader
            While (reader.Read())
                questionText = reader("name_ru")
            End While
        Catch ex As Exception
            MsgBox("Ошибка чтения вопроса порядковый номер " & curQuesIndex & " из БД ID " & id)
        End Try
        Return questionText
    End Function

    Public Sub startNewQuestionare()
        curQuesIndex = 0
        totalRightAnswers = 0
    End Sub

End Module
