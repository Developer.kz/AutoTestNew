﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmActivate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmActivate))
        Me.BtnActivateNew = New System.Windows.Forms.Button()
        Me.BtnCancelNew = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'BtnActivateNew
        '
        Me.BtnActivateNew.BackgroundImage = CType(resources.GetObject("BtnActivateNew.BackgroundImage"), System.Drawing.Image)
        Me.BtnActivateNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnActivateNew.Location = New System.Drawing.Point(327, 497)
        Me.BtnActivateNew.Name = "BtnActivateNew"
        Me.BtnActivateNew.Size = New System.Drawing.Size(241, 82)
        Me.BtnActivateNew.TabIndex = 9
        Me.BtnActivateNew.UseVisualStyleBackColor = True
        '
        'BtnCancelNew
        '
        Me.BtnCancelNew.BackgroundImage = CType(resources.GetObject("BtnCancelNew.BackgroundImage"), System.Drawing.Image)
        Me.BtnCancelNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnCancelNew.Location = New System.Drawing.Point(688, 497)
        Me.BtnCancelNew.Name = "BtnCancelNew"
        Me.BtnCancelNew.Size = New System.Drawing.Size(241, 82)
        Me.BtnCancelNew.TabIndex = 8
        Me.BtnCancelNew.UseVisualStyleBackColor = True
        '
        'FrmActivate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1008, 730)
        Me.Controls.Add(Me.BtnActivateNew)
        Me.Controls.Add(Me.BtnCancelNew)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmActivate"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Активация программы"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BtnActivateNew As Button
    Friend WithEvents BtnCancelNew As Button
End Class
