﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmTest
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmTest))
        Me.tbPnlMain = New System.Windows.Forms.TableLayoutPanel()
        Me.tblPnlCenter = New System.Windows.Forms.TableLayoutPanel()
        Me.TblPnlButtons = New System.Windows.Forms.FlowLayoutPanel()
        Me.BtnNextQuestion = New System.Windows.Forms.Button()
        Me.tblPnlPicture = New System.Windows.Forms.TableLayoutPanel()
        Me.PicBox = New System.Windows.Forms.PictureBox()
        Me.LblQNum = New System.Windows.Forms.Label()
        Me.tblPnlAnswers = New System.Windows.Forms.TableLayoutPanel()
        Me.LAnswer1 = New System.Windows.Forms.Label()
        Me.LAnswer2 = New System.Windows.Forms.Label()
        Me.LAnswer3 = New System.Windows.Forms.Label()
        Me.LAnswer4 = New System.Windows.Forms.Label()
        Me.LAnswer5 = New System.Windows.Forms.Label()
        Me.LAnswer6 = New System.Windows.Forms.Label()
        Me.LblAnswer1 = New System.Windows.Forms.PictureBox()
        Me.LblAnswer2 = New System.Windows.Forms.PictureBox()
        Me.LblAnswer3 = New System.Windows.Forms.PictureBox()
        Me.LblAnswer4 = New System.Windows.Forms.PictureBox()
        Me.LblAnswer5 = New System.Windows.Forms.PictureBox()
        Me.LblAnswer6 = New System.Windows.Forms.PictureBox()
        Me.TblPnlHeader = New System.Windows.Forms.TableLayoutPanel()
        Me.LblQuestion = New System.Windows.Forms.Label()
        Me.LblSystemAnswer = New System.Windows.Forms.Label()
        Me.LblArticle = New System.Windows.Forms.Label()
        Me.TblPnlRight = New System.Windows.Forms.TableLayoutPanel()
        Me.PnlIndicator = New System.Windows.Forms.Panel()
        Me.BtnFinish = New System.Windows.Forms.Button()
        Me.PnlTime = New System.Windows.Forms.Panel()
        Me.LblClock = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.HeaderPic = New System.Windows.Forms.PictureBox()
        Me.tbPnlMain.SuspendLayout()
        Me.tblPnlCenter.SuspendLayout()
        Me.TblPnlButtons.SuspendLayout()
        Me.tblPnlPicture.SuspendLayout()
        CType(Me.PicBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tblPnlAnswers.SuspendLayout()
        CType(Me.LblAnswer1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LblAnswer2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LblAnswer3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LblAnswer4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LblAnswer5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LblAnswer6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TblPnlHeader.SuspendLayout()
        Me.TblPnlRight.SuspendLayout()
        Me.PnlTime.SuspendLayout()
        CType(Me.HeaderPic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbPnlMain
        '
        Me.tbPnlMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbPnlMain.AutoSize = True
        Me.tbPnlMain.BackColor = System.Drawing.Color.Transparent
        Me.tbPnlMain.ColumnCount = 2
        Me.tbPnlMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tbPnlMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150.0!))
        Me.tbPnlMain.Controls.Add(Me.tblPnlCenter, 0, 0)
        Me.tbPnlMain.Controls.Add(Me.TblPnlRight, 1, 0)
        Me.tbPnlMain.Location = New System.Drawing.Point(3, 4)
        Me.tbPnlMain.Name = "tbPnlMain"
        Me.tbPnlMain.RowCount = 1
        Me.tbPnlMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tbPnlMain.Size = New System.Drawing.Size(1176, 735)
        Me.tbPnlMain.TabIndex = 0
        '
        'tblPnlCenter
        '
        Me.tblPnlCenter.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tblPnlCenter.BackColor = System.Drawing.Color.Transparent
        Me.tblPnlCenter.ColumnCount = 1
        Me.tblPnlCenter.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblPnlCenter.Controls.Add(Me.TblPnlButtons, 0, 6)
        Me.tblPnlCenter.Controls.Add(Me.tblPnlPicture, 0, 1)
        Me.tblPnlCenter.Controls.Add(Me.tblPnlAnswers, 0, 3)
        Me.tblPnlCenter.Controls.Add(Me.TblPnlHeader, 0, 0)
        Me.tblPnlCenter.Controls.Add(Me.LblQuestion, 0, 2)
        Me.tblPnlCenter.Controls.Add(Me.LblSystemAnswer, 0, 4)
        Me.tblPnlCenter.Controls.Add(Me.LblArticle, 0, 5)
        Me.tblPnlCenter.Location = New System.Drawing.Point(3, 3)
        Me.tblPnlCenter.Name = "tblPnlCenter"
        Me.tblPnlCenter.RowCount = 7
        Me.tblPnlCenter.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.964378!))
        Me.tblPnlCenter.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.30407!))
        Me.tblPnlCenter.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.781458!))
        Me.tblPnlCenter.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.74985!))
        Me.tblPnlCenter.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33.0!))
        Me.tblPnlCenter.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.200247!))
        Me.tblPnlCenter.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55.0!))
        Me.tblPnlCenter.Size = New System.Drawing.Size(1020, 729)
        Me.tblPnlCenter.TabIndex = 0
        '
        'TblPnlButtons
        '
        Me.TblPnlButtons.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TblPnlButtons.BackColor = System.Drawing.Color.Transparent
        Me.TblPnlButtons.Controls.Add(Me.BtnNextQuestion)
        Me.TblPnlButtons.Location = New System.Drawing.Point(3, 675)
        Me.TblPnlButtons.Name = "TblPnlButtons"
        Me.TblPnlButtons.Size = New System.Drawing.Size(1014, 51)
        Me.TblPnlButtons.TabIndex = 18
        '
        'BtnNextQuestion
        '
        Me.BtnNextQuestion.BackgroundImage = CType(resources.GetObject("BtnNextQuestion.BackgroundImage"), System.Drawing.Image)
        Me.BtnNextQuestion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnNextQuestion.Location = New System.Drawing.Point(3, 3)
        Me.BtnNextQuestion.Name = "BtnNextQuestion"
        Me.BtnNextQuestion.Size = New System.Drawing.Size(191, 51)
        Me.BtnNextQuestion.TabIndex = 0
        Me.BtnNextQuestion.UseVisualStyleBackColor = True
        '
        'tblPnlPicture
        '
        Me.tblPnlPicture.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tblPnlPicture.AutoSize = True
        Me.tblPnlPicture.BackColor = System.Drawing.Color.Transparent
        Me.tblPnlPicture.ColumnCount = 3
        Me.tblPnlPicture.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.tblPnlPicture.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.0!))
        Me.tblPnlPicture.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.tblPnlPicture.Controls.Add(Me.PicBox, 1, 0)
        Me.tblPnlPicture.Controls.Add(Me.LblQNum, 0, 0)
        Me.tblPnlPicture.Location = New System.Drawing.Point(3, 60)
        Me.tblPnlPicture.Name = "tblPnlPicture"
        Me.tblPnlPicture.RowCount = 1
        Me.tblPnlPicture.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblPnlPicture.Size = New System.Drawing.Size(1014, 310)
        Me.tblPnlPicture.TabIndex = 0
        '
        'PicBox
        '
        Me.PicBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PicBox.Image = CType(resources.GetObject("PicBox.Image"), System.Drawing.Image)
        Me.PicBox.Location = New System.Drawing.Point(307, 3)
        Me.PicBox.Name = "PicBox"
        Me.PicBox.Size = New System.Drawing.Size(399, 304)
        Me.PicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PicBox.TabIndex = 1
        Me.PicBox.TabStop = False
        '
        'LblQNum
        '
        Me.LblQNum.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LblQNum.AutoSize = True
        Me.LblQNum.Font = New System.Drawing.Font("Arial Narrow", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LblQNum.ForeColor = System.Drawing.Color.OliveDrab
        Me.LblQNum.Location = New System.Drawing.Point(3, 285)
        Me.LblQNum.Name = "LblQNum"
        Me.LblQNum.Size = New System.Drawing.Size(101, 25)
        Me.LblQNum.TabIndex = 2
        Me.LblQNum.Text = "Вопрос №"
        '
        'tblPnlAnswers
        '
        Me.tblPnlAnswers.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tblPnlAnswers.BackColor = System.Drawing.Color.Transparent
        Me.tblPnlAnswers.ColumnCount = 2
        Me.tblPnlAnswers.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tblPnlAnswers.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblPnlAnswers.Controls.Add(Me.LAnswer1, 1, 0)
        Me.tblPnlAnswers.Controls.Add(Me.LAnswer2, 1, 1)
        Me.tblPnlAnswers.Controls.Add(Me.LAnswer3, 1, 2)
        Me.tblPnlAnswers.Controls.Add(Me.LAnswer4, 1, 3)
        Me.tblPnlAnswers.Controls.Add(Me.LAnswer5, 1, 4)
        Me.tblPnlAnswers.Controls.Add(Me.LAnswer6, 1, 5)
        Me.tblPnlAnswers.Controls.Add(Me.LblAnswer1, 0, 0)
        Me.tblPnlAnswers.Controls.Add(Me.LblAnswer2, 0, 1)
        Me.tblPnlAnswers.Controls.Add(Me.LblAnswer3, 0, 2)
        Me.tblPnlAnswers.Controls.Add(Me.LblAnswer4, 0, 3)
        Me.tblPnlAnswers.Controls.Add(Me.LblAnswer5, 0, 4)
        Me.tblPnlAnswers.Controls.Add(Me.LblAnswer6, 0, 5)
        Me.tblPnlAnswers.Location = New System.Drawing.Point(3, 432)
        Me.tblPnlAnswers.Name = "tblPnlAnswers"
        Me.tblPnlAnswers.RowCount = 6
        Me.tblPnlAnswers.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblPnlAnswers.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblPnlAnswers.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblPnlAnswers.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblPnlAnswers.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblPnlAnswers.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblPnlAnswers.Size = New System.Drawing.Size(1014, 171)
        Me.tblPnlAnswers.TabIndex = 17
        '
        'LAnswer1
        '
        Me.LAnswer1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LAnswer1.AutoSize = True
        Me.LAnswer1.Font = New System.Drawing.Font("Arial Narrow", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LAnswer1.ForeColor = System.Drawing.Color.Black
        Me.LAnswer1.Location = New System.Drawing.Point(43, 2)
        Me.LAnswer1.Name = "LAnswer1"
        Me.LAnswer1.Size = New System.Drawing.Size(178, 25)
        Me.LAnswer1.TabIndex = 16
        Me.LAnswer1.Text = "Вариан ответа № 1"
        '
        'LAnswer2
        '
        Me.LAnswer2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LAnswer2.AutoSize = True
        Me.LAnswer2.Font = New System.Drawing.Font("Arial Narrow", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LAnswer2.ForeColor = System.Drawing.Color.Black
        Me.LAnswer2.Location = New System.Drawing.Point(43, 31)
        Me.LAnswer2.Name = "LAnswer2"
        Me.LAnswer2.Size = New System.Drawing.Size(178, 25)
        Me.LAnswer2.TabIndex = 17
        Me.LAnswer2.Text = "Вариан ответа № 2"
        '
        'LAnswer3
        '
        Me.LAnswer3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LAnswer3.AutoSize = True
        Me.LAnswer3.Font = New System.Drawing.Font("Arial Narrow", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LAnswer3.ForeColor = System.Drawing.Color.Black
        Me.LAnswer3.Location = New System.Drawing.Point(43, 60)
        Me.LAnswer3.Name = "LAnswer3"
        Me.LAnswer3.Size = New System.Drawing.Size(178, 25)
        Me.LAnswer3.TabIndex = 18
        Me.LAnswer3.Text = "Вариан ответа № 3"
        '
        'LAnswer4
        '
        Me.LAnswer4.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LAnswer4.AutoSize = True
        Me.LAnswer4.Font = New System.Drawing.Font("Arial Narrow", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LAnswer4.ForeColor = System.Drawing.Color.Black
        Me.LAnswer4.Location = New System.Drawing.Point(43, 89)
        Me.LAnswer4.Name = "LAnswer4"
        Me.LAnswer4.Size = New System.Drawing.Size(178, 25)
        Me.LAnswer4.TabIndex = 19
        Me.LAnswer4.Text = "Вариан ответа № 4"
        '
        'LAnswer5
        '
        Me.LAnswer5.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LAnswer5.AutoSize = True
        Me.LAnswer5.Font = New System.Drawing.Font("Arial Narrow", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LAnswer5.ForeColor = System.Drawing.Color.Black
        Me.LAnswer5.Location = New System.Drawing.Point(43, 118)
        Me.LAnswer5.Name = "LAnswer5"
        Me.LAnswer5.Size = New System.Drawing.Size(178, 25)
        Me.LAnswer5.TabIndex = 20
        Me.LAnswer5.Text = "Вариан ответа № 5"
        '
        'LAnswer6
        '
        Me.LAnswer6.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LAnswer6.AutoSize = True
        Me.LAnswer6.Font = New System.Drawing.Font("Arial Narrow", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LAnswer6.ForeColor = System.Drawing.Color.Black
        Me.LAnswer6.Location = New System.Drawing.Point(43, 147)
        Me.LAnswer6.Name = "LAnswer6"
        Me.LAnswer6.Size = New System.Drawing.Size(178, 25)
        Me.LAnswer6.TabIndex = 21
        Me.LAnswer6.Text = "Вариан ответа № 6"
        '
        'LblAnswer1
        '
        Me.LblAnswer1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LblAnswer1.Image = CType(resources.GetObject("LblAnswer1.Image"), System.Drawing.Image)
        Me.LblAnswer1.Location = New System.Drawing.Point(3, 3)
        Me.LblAnswer1.Name = "LblAnswer1"
        Me.LblAnswer1.Size = New System.Drawing.Size(23, 23)
        Me.LblAnswer1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LblAnswer1.TabIndex = 22
        Me.LblAnswer1.TabStop = False
        '
        'LblAnswer2
        '
        Me.LblAnswer2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LblAnswer2.BackgroundImage = CType(resources.GetObject("LblAnswer2.BackgroundImage"), System.Drawing.Image)
        Me.LblAnswer2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.LblAnswer2.Image = CType(resources.GetObject("LblAnswer2.Image"), System.Drawing.Image)
        Me.LblAnswer2.Location = New System.Drawing.Point(3, 32)
        Me.LblAnswer2.Name = "LblAnswer2"
        Me.LblAnswer2.Size = New System.Drawing.Size(23, 23)
        Me.LblAnswer2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LblAnswer2.TabIndex = 23
        Me.LblAnswer2.TabStop = False
        '
        'LblAnswer3
        '
        Me.LblAnswer3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LblAnswer3.Image = CType(resources.GetObject("LblAnswer3.Image"), System.Drawing.Image)
        Me.LblAnswer3.Location = New System.Drawing.Point(3, 61)
        Me.LblAnswer3.Name = "LblAnswer3"
        Me.LblAnswer3.Size = New System.Drawing.Size(23, 23)
        Me.LblAnswer3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LblAnswer3.TabIndex = 24
        Me.LblAnswer3.TabStop = False
        '
        'LblAnswer4
        '
        Me.LblAnswer4.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LblAnswer4.Image = CType(resources.GetObject("LblAnswer4.Image"), System.Drawing.Image)
        Me.LblAnswer4.Location = New System.Drawing.Point(3, 90)
        Me.LblAnswer4.Name = "LblAnswer4"
        Me.LblAnswer4.Size = New System.Drawing.Size(23, 23)
        Me.LblAnswer4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LblAnswer4.TabIndex = 25
        Me.LblAnswer4.TabStop = False
        '
        'LblAnswer5
        '
        Me.LblAnswer5.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LblAnswer5.Image = CType(resources.GetObject("LblAnswer5.Image"), System.Drawing.Image)
        Me.LblAnswer5.Location = New System.Drawing.Point(3, 119)
        Me.LblAnswer5.Name = "LblAnswer5"
        Me.LblAnswer5.Size = New System.Drawing.Size(23, 23)
        Me.LblAnswer5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LblAnswer5.TabIndex = 26
        Me.LblAnswer5.TabStop = False
        '
        'LblAnswer6
        '
        Me.LblAnswer6.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.LblAnswer6.Image = CType(resources.GetObject("LblAnswer6.Image"), System.Drawing.Image)
        Me.LblAnswer6.Location = New System.Drawing.Point(3, 148)
        Me.LblAnswer6.Name = "LblAnswer6"
        Me.LblAnswer6.Size = New System.Drawing.Size(23, 23)
        Me.LblAnswer6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LblAnswer6.TabIndex = 27
        Me.LblAnswer6.TabStop = False
        '
        'TblPnlHeader
        '
        Me.TblPnlHeader.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TblPnlHeader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TblPnlHeader.ColumnCount = 1
        Me.TblPnlHeader.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TblPnlHeader.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TblPnlHeader.Controls.Add(Me.HeaderPic, 0, 0)
        Me.TblPnlHeader.Location = New System.Drawing.Point(3, 3)
        Me.TblPnlHeader.Name = "TblPnlHeader"
        Me.TblPnlHeader.RowCount = 1
        Me.TblPnlHeader.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TblPnlHeader.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51.0!))
        Me.TblPnlHeader.Size = New System.Drawing.Size(1014, 51)
        Me.TblPnlHeader.TabIndex = 20
        '
        'LblQuestion
        '
        Me.LblQuestion.AutoSize = True
        Me.LblQuestion.Font = New System.Drawing.Font("Arial Narrow", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.LblQuestion.ForeColor = System.Drawing.Color.Black
        Me.LblQuestion.Location = New System.Drawing.Point(3, 373)
        Me.LblQuestion.Name = "LblQuestion"
        Me.LblQuestion.Size = New System.Drawing.Size(82, 25)
        Me.LblQuestion.TabIndex = 21
        Me.LblQuestion.Text = " Вопрос"
        '
        'LblSystemAnswer
        '
        Me.LblSystemAnswer.AutoSize = True
        Me.LblSystemAnswer.Font = New System.Drawing.Font("Arial Narrow", 15.75!, System.Drawing.FontStyle.Bold)
        Me.LblSystemAnswer.ForeColor = System.Drawing.Color.OliveDrab
        Me.LblSystemAnswer.Location = New System.Drawing.Point(3, 606)
        Me.LblSystemAnswer.Name = "LblSystemAnswer"
        Me.LblSystemAnswer.Size = New System.Drawing.Size(144, 25)
        Me.LblSystemAnswer.TabIndex = 22
        Me.LblSystemAnswer.Text = "Ответ системы"
        '
        'LblArticle
        '
        Me.LblArticle.AutoSize = True
        Me.LblArticle.Font = New System.Drawing.Font("Arial Narrow", 15.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.LblArticle.ForeColor = System.Drawing.Color.CornflowerBlue
        Me.LblArticle.Location = New System.Drawing.Point(3, 639)
        Me.LblArticle.Name = "LblArticle"
        Me.LblArticle.Size = New System.Drawing.Size(155, 25)
        Me.LblArticle.TabIndex = 23
        Me.LblArticle.Text = "Текст подсказки"
        '
        'TblPnlRight
        '
        Me.TblPnlRight.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TblPnlRight.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(44, Byte), Integer))
        Me.TblPnlRight.ColumnCount = 1
        Me.TblPnlRight.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TblPnlRight.Controls.Add(Me.PnlIndicator, 0, 1)
        Me.TblPnlRight.Controls.Add(Me.BtnFinish, 0, 2)
        Me.TblPnlRight.Controls.Add(Me.PnlTime, 0, 0)
        Me.TblPnlRight.Location = New System.Drawing.Point(1029, 3)
        Me.TblPnlRight.Name = "TblPnlRight"
        Me.TblPnlRight.RowCount = 3
        Me.TblPnlRight.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60.0!))
        Me.TblPnlRight.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TblPnlRight.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51.0!))
        Me.TblPnlRight.Size = New System.Drawing.Size(144, 729)
        Me.TblPnlRight.TabIndex = 1
        '
        'PnlIndicator
        '
        Me.PnlIndicator.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PnlIndicator.BackColor = System.Drawing.Color.FromArgb(CType(CType(61, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(44, Byte), Integer))
        Me.PnlIndicator.Location = New System.Drawing.Point(3, 63)
        Me.PnlIndicator.Name = "PnlIndicator"
        Me.PnlIndicator.Size = New System.Drawing.Size(138, 612)
        Me.PnlIndicator.TabIndex = 0
        '
        'BtnFinish
        '
        Me.BtnFinish.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnFinish.BackgroundImage = CType(resources.GetObject("BtnFinish.BackgroundImage"), System.Drawing.Image)
        Me.BtnFinish.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BtnFinish.Location = New System.Drawing.Point(3, 681)
        Me.BtnFinish.Name = "BtnFinish"
        Me.BtnFinish.Size = New System.Drawing.Size(138, 45)
        Me.BtnFinish.TabIndex = 1
        Me.BtnFinish.UseVisualStyleBackColor = True
        '
        'PnlTime
        '
        Me.PnlTime.BackColor = System.Drawing.Color.Transparent
        Me.PnlTime.BackgroundImage = CType(resources.GetObject("PnlTime.BackgroundImage"), System.Drawing.Image)
        Me.PnlTime.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PnlTime.Controls.Add(Me.LblClock)
        Me.PnlTime.Location = New System.Drawing.Point(3, 3)
        Me.PnlTime.Name = "PnlTime"
        Me.PnlTime.Size = New System.Drawing.Size(138, 54)
        Me.PnlTime.TabIndex = 2
        '
        'LblClock
        '
        Me.LblClock.AutoSize = True
        Me.LblClock.Font = New System.Drawing.Font("Arial Narrow", 24.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.LblClock.ForeColor = System.Drawing.Color.Chartreuse
        Me.LblClock.Location = New System.Drawing.Point(27, 18)
        Me.LblClock.Name = "LblClock"
        Me.LblClock.Size = New System.Drawing.Size(86, 37)
        Me.LblClock.TabIndex = 0
        Me.LblClock.Text = "10:15"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "gray_box.jpg")
        Me.ImageList1.Images.SetKeyName(1, "green_box.jpg")
        Me.ImageList1.Images.SetKeyName(2, "red_box.jpg")
        Me.ImageList1.Images.SetKeyName(3, "clocks.png")
        '
        'HeaderPic
        '
        Me.HeaderPic.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.HeaderPic.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.HeaderPic.Location = New System.Drawing.Point(3, 3)
        Me.HeaderPic.Name = "HeaderPic"
        Me.HeaderPic.Size = New System.Drawing.Size(1008, 45)
        Me.HeaderPic.TabIndex = 0
        Me.HeaderPic.TabStop = False
        '
        'FrmMaxTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1184, 742)
        Me.Controls.Add(Me.tbPnlMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmMaxTest"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmMaxTest"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tbPnlMain.ResumeLayout(False)
        Me.tblPnlCenter.ResumeLayout(False)
        Me.tblPnlCenter.PerformLayout()
        Me.TblPnlButtons.ResumeLayout(False)
        Me.tblPnlPicture.ResumeLayout(False)
        Me.tblPnlPicture.PerformLayout()
        CType(Me.PicBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tblPnlAnswers.ResumeLayout(False)
        Me.tblPnlAnswers.PerformLayout()
        CType(Me.LblAnswer1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LblAnswer2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LblAnswer3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LblAnswer4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LblAnswer5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LblAnswer6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TblPnlHeader.ResumeLayout(False)
        Me.TblPnlRight.ResumeLayout(False)
        Me.PnlTime.ResumeLayout(False)
        Me.PnlTime.PerformLayout()
        CType(Me.HeaderPic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents tbPnlMain As TableLayoutPanel
    Friend WithEvents tblPnlCenter As TableLayoutPanel
    Friend WithEvents tblPnlPicture As TableLayoutPanel
    Friend WithEvents PicBox As PictureBox
    Friend WithEvents tblPnlAnswers As TableLayoutPanel
    Friend WithEvents Timer1 As Timer
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents TblPnlButtons As FlowLayoutPanel
    Friend WithEvents TblPnlHeader As TableLayoutPanel
    Friend WithEvents LblQuestion As Label
    Friend WithEvents LAnswer1 As Label
    Friend WithEvents LAnswer2 As Label
    Friend WithEvents LAnswer3 As Label
    Friend WithEvents LAnswer4 As Label
    Friend WithEvents LAnswer5 As Label
    Friend WithEvents LAnswer6 As Label
    Friend WithEvents LblSystemAnswer As Label
    Friend WithEvents LblArticle As Label
    Friend WithEvents BtnNextQuestion As Button
    Friend WithEvents LblAnswer1 As PictureBox
    Friend WithEvents LblAnswer2 As PictureBox
    Friend WithEvents LblAnswer3 As PictureBox
    Friend WithEvents LblAnswer4 As PictureBox
    Friend WithEvents LblAnswer5 As PictureBox
    Friend WithEvents LblAnswer6 As PictureBox
    Friend WithEvents TblPnlRight As TableLayoutPanel
    Friend WithEvents PnlIndicator As Panel
    Friend WithEvents BtnFinish As Button
    Friend WithEvents PnlTime As Panel
    Friend WithEvents LblClock As Label
    Friend WithEvents LblQNum As Label
    Friend WithEvents HeaderPic As PictureBox
End Class
