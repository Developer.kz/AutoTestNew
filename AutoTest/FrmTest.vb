﻿Imports System.IO

Public Class FrmTest
    Private minutes As Integer
    Private seconds As Integer
    Private Const MaxMinutes As Integer = 40
    Private Wrongs As Integer
    Private Const WrongsLimit As Integer = 9
    Private ArticleNum As String

    Private Sub FrmMaxTest_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        FrmMain.Show()
    End Sub

    Private Sub FrmMaxTest_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.FormBorderStyle = FormBorderStyle.None
        Me.WindowState = FormWindowState.Maximized
        If Module1.isExam Then
            HeaderPic.BackgroundImage = Image.FromFile(picpath + "HeaderExam.jpg")
        Else
            HeaderPic.BackgroundImage = Image.FromFile(picpath + "HeaderTest.jpg")
        End If
        HideAnswers()
        Module1.ReDimArrQuestions(Module1.testMaxQues)
        Wrongs = 0

        Me.Text = "ТЕСТОВЫЙ РЕЖИМ"
        Module1.startNewQuestionare()
        Module1.getCurrentQuestion()
        Module1.GetAnswers()
        ShowQuestion()
        If Module1.isExam = True Then
            LblClock.Text = CStr(minutes) & ":" & CStr(seconds)
            Me.Text = "ЭКЗАМЕН"
            minutes = MaxMinutes - 1
            seconds = 60
            Timer1.Enabled = True
            PnlTime.Show()
            PaintIndicatorPic()
        Else
            PaintIndicatorPic()
            PnlTime.Hide()
        End If

    End Sub
    Private Sub FrmMaxTest_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown

    End Sub
    Private Sub ShowAnswer(k As Integer, txt As String)
        If k = 1 Then
            'LAnswer1.Text = "1) " & txt
            LAnswer1.Text = txt
            LAnswer1.Show()
            LblAnswer1.Show()
            LblAnswer1.Image = Image.FromFile(picpath + "BtnChecked.jpg")
        End If
        If k = 2 Then
            'LAnswer2.Text = "2) " & txt
            LAnswer2.Text = txt
            LAnswer2.Show()
            LblAnswer2.Show()
            LblAnswer2.Image = Image.FromFile(picpath + "BtnChecked.jpg")
        End If
        If k = 3 Then
            'LAnswer3.Text = "3) " & txt
            LAnswer3.Text = txt
            LAnswer3.Show()
            LblAnswer3.Show()
            LblAnswer3.Image = Image.FromFile(picpath + "BtnChecked.jpg")
        End If
        If k = 4 Then
            'LAnswer4.Text = "4) " & txt
            LAnswer4.Text = txt
            LAnswer4.Show()
            LblAnswer4.Show()
            LblAnswer4.Image = Image.FromFile(picpath + "BtnChecked.jpg")
        End If
        If k = 5 Then
            LAnswer5.Show()
            'LAnswer5.Text = "5) " & txt
            LAnswer5.Text = txt
            LblAnswer5.Show()
            LblAnswer5.Image = Image.FromFile(picpath + "BtnChecked.jpg")
        End If
        If k = 6 Then
            LAnswer6.Show()
            'LAnswer6.Text = "6) " & txt
            LAnswer6.Text = txt
            LblAnswer6.Show()
            LblAnswer6.Image = Image.FromFile(picpath + "BtnChecked.jpg")
        End If
    End Sub
    Private Sub HideAnswers()
        LAnswer1.Hide()
        LAnswer2.Hide()
        LAnswer3.Hide()
        LAnswer4.Hide()
        LAnswer5.Hide()
        LAnswer6.Hide()
        LblAnswer1.Hide()
        LblAnswer2.Hide()
        LblAnswer3.Hide()
        LblAnswer4.Hide()
        LblAnswer5.Hide()
        LblAnswer6.Hide()
        LblSystemAnswer.Hide()
        LblArticle.Hide()
        BtnNextQuestion.Hide()
    End Sub

    Private Sub BtnNextQuestion_Click(sender As Object, e As EventArgs)
        GoNextQuestion()
    End Sub

    Private Sub ShowQuestion()
        HideAnswers()
        LblQNum.Text = "Вопрос №" & Convert.ToString(Module1.curQuesIndex)
        LblQuestion.Text = Module1.getCurrentQuestion()
        If (Module1.PicExistsQuestion(Module1.curQuesIndex)) Then
            PicBox.Image = Image.FromStream(Module1.GetPicFromDb(Module1.curQuesIndex))
        Else
            PicBox.Image = Image.FromFile(picpath + "nopic.jpg")
        End If
        Dim i As Integer
        i = 0
        While i < Module1.answersArr.Count
            ShowAnswer(i, Module1.answersArr(i))
            i += 1
        End While
        LblSystemAnswer.Text = ""
        LblArticle.Text = ""
        LblArticle.Show()
        LblSystemAnswer.Show()
    End Sub

    Private Sub ShowArticle(right_id As Integer)
        'определение номера правильного ответа по ID
        LblSystemAnswer.Text = "Не правильно, правильный ответ № " & GetRightAnswerNum(right_id)
        ArticleNum = GetArticle(Module1.curQuesId)
        If String.IsNullOrEmpty(ArticleNum) = False Then
            LblArticle.Text = "Нажмите пункт " + ArticleNum
        Else
            LblArticle.Text = ""
        End If

        If Module1.curQuesIndex < Module1.testMaxQues Then
            BtnNextQuestion.Show()
        End If
        If Module1.curQuesIndex = Module1.testMaxQues - 1 Then
            EndTest()
        End If
    End Sub

    Function GetRightAnswerNum(right_answer_id) As Integer
        Dim k As Integer
        k = 0
        If Module1.answersIdArr(1) = right_answer_id Then
            k = 1
            Return k
        End If
        If Module1.answersIdArr(2) = right_answer_id Then
            k = 2
            Return k
        End If
        If Module1.answersIdArr(3) = right_answer_id Then
            k = 3
            Return k
        End If
        If Module1.answersIdArr(4) = right_answer_id Then
            k = 4
            Return k
        End If
        If Module1.answersIdArr(5) = right_answer_id Then
            k = 5
            Return k
        End If
        If Module1.answersIdArr(6) = right_answer_id Then
            k = 6
            Return k
        End If
        Return k
    End Function
    Private Sub PaintRed(PQnumber As Integer)
        Dim index As Integer = PQnumber + 1
        Dim pb As PictureBox = CType(Me.PnlIndicator.Controls("cipher" & CStr(index)), PictureBox)
        pb.Image = Image.FromFile(picpath + "ciphers\" + CStr(index) + "_R.jpg")
    End Sub
    Private Sub PaintGreen(PQnumber As Integer)
        Dim index As Integer = PQnumber + 1
        Dim pb As PictureBox = CType(Me.PnlIndicator.Controls("cipher" & CStr(index)), PictureBox)
        pb.Image = Image.FromFile(picpath + "ciphers\" + CStr(index) + "_G.jpg")
    End Sub
    Private Sub PaintWhite(PQnumber As Integer)
        Dim index As Integer = PQnumber + 1
        Dim pb As PictureBox = CType(Me.PnlIndicator.Controls("cipher" & CStr(index)), PictureBox)
        pb.Image = Image.FromFile(picpath + "ciphers\" + CStr(index) + "_W.jpg")
    End Sub
    Private Sub PostAnswer(answeredId As Integer)
        Dim right_id As Integer
        right_id = GetRightAnswerId(Module1.curQuesIndex)
        'если не правильно ответил
        If right_id <> answeredId Then
            Wrongs += 1
            PaintRed(Module1.curQuesIndex)
            If Module1.isExam = False Then
                ShowArticle(right_id)
                Return
            End If
        Else
            Module1.totalRightAnswers += 1
            PaintGreen(Module1.curQuesIndex)
        End If
        If Module1.isExam = True Then
            If Wrongs = WrongsLimit Then
                If Module1.isProd = True Then
                    EndTest()
                End If
            End If
            GoNextQuestion()
        End If
    End Sub
    'перейти к следующему вопросу
    Private Sub GoNextQuestion()
        If Module1.curQuesIndex = Module1.testMaxQues - 1 Then
            EndTest()
        End If

        Module1.questionForward()
        Module1.getCurrentQuestion()
        Module1.GetAnswers()
        ShowQuestion()

    End Sub
    'окончание теста
    Private Sub EndTest()
        If Module1.isExam = True Then
            If Wrongs = WrongsLimit Then
                MsgBox("Экзамен не сдан. Вы допустили " & CStr(Wrongs) & " ошибок.")
            Else
                MsgBox("Экзамен окончен. Количество правильных ответов - " & Module1.totalRightAnswers & " из " & CStr(Module1.testMaxQues) & " вопросов.")
            End If
            Timer1.Stop()
            Timer1.Enabled = False
        Else
            MsgBox("Тест окончен. Количество правильных ответов - " & Module1.totalRightAnswers & " из " & CStr(Module1.testMaxQues) & " вопросов.")
        End If
        Hide()
    End Sub
    'отображение индикатора ответов
    Private Sub PaintIndicatorPic()
        Dim k As Integer
        Dim offset As Integer
        offset = 0
        k = 1
        Dim wid As Integer
        Dim heig As Integer
        wid = PnlIndicator.Width / 2
        heig = (PnlIndicator.Height - 20) / 20
        For i As Integer = 1 To Module1.testMaxQues
            Dim pb As New PictureBox
            pb.Image = Image.FromFile(picpath + "ciphers\" + CStr(k) + ".jpg")
            pb.Name = "cipher" & k
            pb.SizeMode = PictureBoxSizeMode.StretchImage
            pb.Width = wid
            pb.Height = heig
            If i = 21 Then
                offset = 0
            End If
            If i > 20 Then
                pb.Top = (i - 20) + offset
                pb.Left = wid
            Else
                pb.Top = i + offset
                pb.Left = 1
            End If
            offset += heig
            Me.PnlIndicator.Controls.Add(pb)
            k += 1
        Next
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If seconds < 61 And seconds > 0 Then
            seconds -= 1
        Else
            seconds = 60
            If minutes < MaxMinutes + 1 And minutes > 0 Then
                minutes -= 1
            Else
                If Module1.isExam = True Then
                    If minutes = 0 And seconds = 0 Then
                        EndTest()
                    End If
                End If
            End If
        End If
        LblClock.Text = CStr(minutes) & ":" & CStr(seconds)
        'Application.DoEvents()
    End Sub
    Private Sub BtnNextQuestion_Click_1(sender As Object, e As EventArgs)
        GoNextQuestion()
    End Sub

    Private Sub BtnNextQuestion_MouseDown(sender As Object, e As MouseEventArgs) Handles BtnNextQuestion.MouseDown
        BtnNextQuestion.BackgroundImage = Image.FromFile(picpath + "BtnNextPressed.jpg")
    End Sub

    Private Sub BtnNextQuestion_MouseUp(sender As Object, e As MouseEventArgs) Handles BtnNextQuestion.MouseUp
        BtnNextQuestion.BackgroundImage = Image.FromFile(picpath + "BtnNext.jpg")
        GoNextQuestion()
    End Sub

    Private Sub LblAnswer1_Click(sender As Object, e As EventArgs) Handles LblAnswer1.Click
        LblAnswer1.Image = Image.FromFile(picpath + "BtnCheckedPressed.jpg")
        PostAnswer(Module1.answersIdArr(1))
    End Sub

    Private Sub LblAnswer2_Click(sender As Object, e As EventArgs) Handles LblAnswer2.Click
        LblAnswer2.Image = Image.FromFile(picpath + "BtnCheckedPressed.jpg")
        PostAnswer(Module1.answersIdArr(2))
    End Sub

    Private Sub LblAnswer3_Click(sender As Object, e As EventArgs) Handles LblAnswer3.Click
        LblAnswer3.Image = Image.FromFile(picpath + "BtnCheckedPressed.jpg")
        PostAnswer(Module1.answersIdArr(3))
    End Sub

    Private Sub LblAnswer4_Click(sender As Object, e As EventArgs) Handles LblAnswer4.Click
        LblAnswer4.Image = Image.FromFile(picpath + "BtnCheckedPressed.jpg")
        PostAnswer(Module1.answersIdArr(4))
    End Sub
    Private Sub LblAnswer5_Click(sender As Object, e As EventArgs) Handles LblAnswer5.Click
        LblAnswer5.Image = Image.FromFile(picpath + "BtnCheckedPressed.jpg")
        PostAnswer(Module1.answersIdArr(5))
    End Sub

    Private Sub LblAnswer6_Click(sender As Object, e As EventArgs) Handles LblAnswer6.Click
        LblAnswer6.Image = Image.FromFile(picpath + "BtnCheckedPressed.jpg")
        PostAnswer(Module1.answersIdArr(6))
    End Sub

    Private Sub BtnFinish_MouseDown(sender As Object, e As MouseEventArgs) Handles BtnFinish.MouseDown
        BtnFinish.BackgroundImage = Image.FromFile(picpath + "BtnEndPressed.jpg")
    End Sub

    Private Sub BtnFinish_MouseUp(sender As Object, e As MouseEventArgs) Handles BtnFinish.MouseUp
        BtnFinish.BackgroundImage = Image.FromFile(picpath + "BtnEnd.jpg")
        EndTest()
    End Sub
    Private Sub LblArticle_MouseEnter(sender As Object, e As EventArgs) Handles LblArticle.MouseEnter
        LblArticle.Cursor = Cursors.Hand
    End Sub

    Private Sub LblArticle_Click(sender As Object, e As EventArgs) Handles LblArticle.Click
        If String.IsNullOrEmpty(Module1.htmlFile) = False Then
            FrmArticle.wb.Navigate(Module1.htmlpath & Module1.htmlFile & "#" & ArticleNum)
            If Module1.htmlFile = "pdd.html" Then
                FrmArticle.Text = "Правила дорожного движения"
            End If
            If Module1.htmlFile = "znaki.html" Then
                FrmArticle.Text = "Дорожные знаки"
            End If
            If Module1.htmlFile = "expl.html" Then
                FrmArticle.Text = "Основные положения по допуску транспортных средств к эксплуатации"
            End If
            If Module1.htmlFile = "per.html" Then
                FrmArticle.Text = "ПЕРЕЧЕНЬ неисправностей и условий, создающих угрозу безопасности дорожного движения и окружающей среде, при которых запрещается эксплуатация транспортных средств"
            End If
            If Module1.htmlFile = "adm.html" Then
                FrmArticle.Text = "КОДЕКС РЕСПУБЛИКИ КАЗАХСТАН ОБ АДМИНИСТРАТИВНЫХ ПРАВОНАРУШЕНИЯХ"
            End If
            If Module1.htmlFile = "osnovy.html" Then
                FrmArticle.Text = "Основы безопасности дорожного движения"
            End If
            If Module1.htmlFile = "med.html" Then
                FrmArticle.Text = "Оказание первой медицинской помощи"
            End If
            FrmArticle.ShowDialog()
        End If
    End Sub
End Class