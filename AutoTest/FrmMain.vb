﻿Imports System.ComponentModel

Public Class FrmMain
    Private Sub BtnStartTest_Click(sender As Object, e As EventArgs)
        Module1.isExam = False
        Me.Hide()
        FrmTest.Show()
    End Sub
    Private Sub BtnStartExam_Click(sender As Object, e As EventArgs)
        Module1.isExam = True
        Me.Hide()
        FrmTest.Show()
    End Sub
    Private Sub FrmMain_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Module1.CloseConn()
        Application.Exit()
    End Sub

    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton1.Click
        Me.Hide()
        FrmImport.Show()
    End Sub

    Private Sub FrmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.BackgroundImage = Image.FromFile(picpath + "splash.jpg")
    End Sub

    Private Sub BtnTestNew_MouseDown(sender As Object, e As MouseEventArgs) Handles BtnTestNew.MouseDown
        BtnTestNew.BackgroundImage = Image.FromFile(picpath + "BtnTestPressed.jpg")
    End Sub

    Private Sub BtnTestNew_MouseUp(sender As Object, e As MouseEventArgs) Handles BtnTestNew.MouseUp
        BtnTestNew.BackgroundImage = Image.FromFile(picpath + "BtnTest.jpg")
        Module1.isExam = False
        Me.Hide()
        FrmTest.Show()
    End Sub

    Private Sub BtnExamNew_MouseDown(sender As Object, e As MouseEventArgs) Handles BtnExamNew.MouseDown
        BtnExamNew.BackgroundImage = Image.FromFile(picpath + "BtnExamPressed.jpg")
    End Sub

    Private Sub BtnExamNew_MouseUp(sender As Object, e As MouseEventArgs) Handles BtnExamNew.MouseUp
        BtnExamNew.BackgroundImage = Image.FromFile(picpath + "BtnExam.jpg")
        Module1.isExam = True
        Me.Hide()
        FrmTest.Show()
    End Sub
End Class