Примечания разработчка, Токбаев Жасулан, Алматы 2018.

Среда разработки VisualStudio 2015 Ent. БД SQLite3. Совместимость Windows 7 и выше.

Чтобы работать в режиме теста (разработчика) надо разблокировать соответствующие строки в Module1.vb:

    'путь к БД в режиме разработки
    'Private dbpath As String = "URI=file:d:\dev\prj\AutoTestNew\autotest_db.db;"
    'Public picpath As String = "d:\dev\prj\AutoTestNew\AutoTest\pic\"
    'Public htmlpath As String = "d:\dev\prj\AutoTestNew\AutoTest\html\"

    'путь к БД в режиме продуктива
    Private dbpath As String = "URI=file:" & Application.StartupPath & "\db\autotest_db.db;"
    Public picpath As String = Application.StartupPath & "\pic\"
    Public htmlpath As String = Application.StartupPath & "\html\"

Менять данные в БД надо в файле autotest_db.db в корне, при сборке скопировать поверх .\AutoTest\db\.

Для сборки дистрибутива встроенным инструментом  ClickOnce (команда Build->Publish) надо скопировать файлы:
    "materials\NDP452-KB2901907-x86-x64-AllOS-ENU.exe"    в папку "D:\dev\mvs14\SDK\Bootstrapper\Packages\DotNetFX452\",
    "materials\NDP452-KB2901907-x86-x64-AllOS-RUS.exe"   в папку "D:\dev\mvs14\SDK\Bootstrapper\Packages\DotNetFX452\ru".

Также проверить в свойствах проекта в разделе Publish кнопка Prerequesities  параметр "Download from same as my application".

    
Устанавливается приложение примерно по этому пути C:\Users\zhassulan.tokbayev\AppData\Local\Apps\2.0\4K5A2G1H.QOA\1403G2E8.RYQ\auto..tion_0000000000000000_0001.0000_4255ce34d97f85f8. 
Значок появляется на рабочем столе.
Перед повторной установкой обязательно надо удалить из панели управление старое приложение.
    
    
